package com.assis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.assis.entity.Contato;

public interface PessoaRepositorio extends JpaRepository<Contato, Long> {

}
