package com.assis.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assis.dao.PessoaRepositorio;
import com.assis.entity.Contato;

import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.DataFetcher;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;

@RestController
public class ContatoController {

	@Autowired
	private PessoaRepositorio repositorio;

	@Value("classpath:contato.graphqls")
	private Resource schemaResource;

	private GraphQL graphQL;

	@PostConstruct
	public void loadSchema() throws IOException {
		File schemaFile = schemaResource.getFile();
		TypeDefinitionRegistry registry = new SchemaParser().parse(schemaFile);
		RuntimeWiring wiring = buildWiring();
		GraphQLSchema schema = new SchemaGenerator().makeExecutableSchema(registry, wiring);
		graphQL = GraphQL.newGraphQL(schema).build();
	}

	private RuntimeWiring buildWiring() {
		DataFetcher<List<Contato>> fetcher1 = data -> {
			return repositorio.findAll();
		};

		return RuntimeWiring.newRuntimeWiring().type("Query",
				typeWiring -> typeWiring.dataFetcher("todosContatos", fetcher1)).build();
	}

	/**
	 * Persiste os contatos
	 * @return Mensagem com a quantidade de contatos criados
	 */
	@PostMapping("/adicionarContatos")
	public String adicionarContatos() {
		Integer qtdContatosCriados = this.criarContatos();
		return qtdContatosCriados + " contatos criados com sucesso";
	}

	/**
	 * Busca todos os contatos em uma busca normal no padrão REST
	 * @return
	 */
	@GetMapping("/buscarContatos")
	public List<Contato> buscarContatos() {
		return this.repositorio.findAll();
	}

	/**
	 * Busca contatos com os filtros GraphQL
	 * @param query
	 * @return
	 */
	@PostMapping("/todosContatos")
	public ResponseEntity<Object> todosContatos(@RequestBody String query) {
		ExecutionResult result = graphQL.execute(query);
		return new ResponseEntity<Object>(result, HttpStatus.OK);
	}
	
	/**
	 * Cria e persiste todos os contatos para teste
	 * @return Quantidade de contatos persistidos
	 */
	private Integer criarContatos() {
		List<Contato> contatos = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			Contato c = new Contato();
			c.setNome("Contato " + i);
			c.setEmail("contato" + i + "@infoway-pi.com.br");
			c.setEndereco("Endereço do contato " + i);
			contatos.add(c);
		}
		this.repositorio.saveAll(contatos);
		return contatos.size();
	}

}
